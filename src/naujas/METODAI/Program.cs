﻿
using System.Reflection.PortableExecutable;

public class Program
{
    public static void Main()
    {

        //Console.Write("Stulpeliu kiekis :");
        //var colCount = int.Parse(Console.ReadLine());

        //Console.Write("Eiluciu kiekis :");
        //var rowCount = int.Parse(Console.ReadLine());

        var colCount = ReadDimension("Stulpeliu kiekis : ");
        var rowCount = ReadDimension("Eiluciu kiekis : ");

        var matrix = ReadMatrix(colCount, rowCount);
        PrintMatrix(matrix);

    }
    public static int ReadDimension(string title)
    {
        Console.Write(title);
        return int.Parse(Console.ReadLine());


    }
    public static int[,] ReadMatrix(int colCount, int rowCount)
    {
        var matrix = new int[colCount, rowCount];

        for (int row = 0; row < rowCount; row++)
        {

            for (int col = 0; col < colCount; col++)
            {
                Console.Write($"Iveskite {row + 1} eilutes {col + 1} reiksmes");

                matrix[col, row] = int.Parse(Console.ReadLine());

            }
        }

        return matrix;
    }
    public static void PrintMatrix(int[,] matrix)
    {
        var colCount = matrix.GetLength(0);
        var rowCount = matrix.GetLength(1);

        for (int row = 0; row < rowCount; row++)
        {
            for (int col = 0; col < colCount ; col++)
            {
                Console.Write($"\t{matrix[col, row]}");
            }
            Console.WriteLine();
        }

    }
}






