﻿
using geofigures;
using System;
namespace GeoFigures
{
    public  class Triangle : BaseFigure
    {
        public decimal Base { get; private set; }
        public decimal Height { get; private set; }

        public Triangle(decimal base1, decimal height1)
        {
            if (base1 > 0)
                Base = base1;
            if (height1 > 0)
                Height = height1; 
        }

        public virtual decimal GetArea() => (Base/2) * Height;

        public override decimal GetPerimeter() => 0;


    }
}       