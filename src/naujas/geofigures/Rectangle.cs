﻿
using geofigures;
using System;
namespace GeoFigures
{
    public  class Rectangle : BaseFigure
    {
        public decimal Width { get; private set; }
        public decimal Height { get; private set; }

        public Rectangle(decimal width, decimal height)
        {
            if (width > 0)
                Width = width;
            if (height > 0)
                Height = height;
        }

        public virtual decimal GetArea() => Width * Height;

        public override decimal GetPerimeter() => (Width + Height) * 2;
        

    }
}