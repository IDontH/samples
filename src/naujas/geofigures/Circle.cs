﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace geofigures
{
    public class Circle : BaseFigure
    {
        public decimal Radius { get; private set; } 

        public Circle(decimal radius)
        {
            if (radius > 0)
                Radius = radius;    

        }

        public virtual decimal GetArea() => (decimal)Math.PI * Radius * Radius;

        public override decimal GetPerimeter() => 2 * (decimal)Math.PI * Radius;

    }
}
