﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace geofigures
{
    public abstract class BaseFigure
    {
        public virtual decimal GetArea() => 0;

        public abstract decimal GetPerimeter();
    }
}
