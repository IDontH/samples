﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Security.Authentication.ExtendedProtection;
using System.Text;
using System.Threading.Tasks;

namespace CLASS
{
    internal class Person
    {
        private const int PersonalCodeLength = 11;


        public string FirstName { get; set; }
        public string LastName { get; set; }

        private int _height;
        public int Height
        {
            get => _height;
            set
            {
                if (value > 0)
                    _height = value;
            }
        }

        private DateTime _birthday;
        public DateTime Birthday
        {
            get => _birthday;
            set
            {
                if (value < DateTime.UtcNow)
                    _birthday = value;
            }
        }

        private float _weight;
        public float Weight
        {
            get => _weight;
            set
            {
                if (value > 0)
                    _weight = value;
            }
        }

        private long _personalcode;
        public long PersonalCode
        {
            get => _personalcode;
            set
            {
                if (value.ToString().Length == PersonalCodeLength)
                {
                    var checksumremainder = CalculateCheckRemainder(value, 0);

                    if (checksumremainder == 10)
                        checksumremainder = CalculateCheckRemainder(value, 2);

                    if (checksumremainder == value % 10)
                    {
                        _personalcode = value;
                        Console.WriteLine("teisingas");
                    }
                    else
                        Console.WriteLine($"neteisingas ({checksumremainder})");

                    _personalcode = value;
                }



            }
        }

        public Person(string firstName, string lastName)
        {
            FirstName = firstName;
            LastName = lastName;    
        }
        
        public void SayHello()
        {
            Console.WriteLine($"vardas : {FirstName}");
        }

        private long CalculateCheckRemainder(long personalCode, int shift)
        {
            long checksum = 0;
            for (int i = 0; i < PersonalCodeLength - 1; i++)
            {

                var rev = PersonalCodeLength - i;
                var digit = (personalCode % (long)Math.Pow(10, rev) / (long)Math.Pow(10, rev - 1));
                var mult = (i+ shift) + 1;

                checksum += digit * mult;


            }

            return checksum % PersonalCodeLength;
        }
    }
}
