﻿

public class Program
{
    public static void Main()
    {
        var colCount = ReadDimension("Stulpeliu kiekis : ");
        var rowCount = ReadDimension("Eiluciu kiekis : ");
        
        Console.WriteLine("Matrix1");
        var matrix1 = ReadMatrix(colCount, rowCount);
        Console.WriteLine("Matrix2");
        var matrix2 = ReadMatrix(colCount, rowCount);

        var mulMatrix = MultiplyMatrix(matrix1, matrix2);
        PrintMatrix(mulMatrix);
    }



    public static int ReadDimension(string title)
    {
        Console.Write(title);
        return int.Parse(Console.ReadLine());


    }
    public static int[,] ReadMatrix(int colCount, int rowCount)
    {
        var matrix = new int[colCount, rowCount];

        for (int row = 0; row < rowCount; row++)
        {

            for (int col = 0; col < colCount; col++)
            {
                Console.Write($"Iveskite {row + 1} eilutes {col + 1} reiksmes: ");

                matrix[col, row] = int.Parse(Console.ReadLine());

            }
        }

        return matrix;
    }
   
    public static int[,] MultiplyMatrix(int[,] matrix1, int[,] matrix2)
    {
        var colCount = matrix1.GetLength(0);
        var rowCount = matrix1.GetLength(1);

        var mulMatrix = new int[colCount, rowCount];
        for (int row = 0; row < rowCount; row++)
        {
            for (int col = 0; col < colCount; col++)
            {
                mulMatrix[col, row] = matrix1[col, row] + matrix2[col, row];
            }
        }
        return mulMatrix;
    }
//    public static int[,] SumMatrix(int[,] matrix1, int[,] matrix2)
//{
//    var colCount = matrix1.GetLength(0);
//    var rowCount = matrix1.GetLength(1);

//    var sumMatrix = new int[colCount, rowCount];
//    for (int row = 0; row < rowCount; row++)
//    {
//        for (int col = 0; col < colCount; col++)
//        {
//            sumMatrix[col, row] = matrix1[col, row] * matrix2[col, row];
//        }
//    }
//    return sumMatrix;
//}
    public static void PrintMatrix(int[,] matrix)
    {
        var colCount = matrix.GetLength(0);
        var rowCount = matrix.GetLength(1);

        for (int row = 0; row < rowCount; row++)
        {
            for (int col = 0; col < colCount; col++)
            {
                Console.Write($"\t{matrix[col, row]}");
            }
            Console.WriteLine();
        }

    }

    
}

   

