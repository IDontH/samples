﻿Console.BackgroundColor = System.ConsoleColor.DarkGreen;

Console.Write("NUMERIS A: ");
var a = Console.ReadLine();

Console.Write("NUMERIS B: ");
var b = Console.ReadLine();

var numA = int.Parse(a);
var numB = int.Parse(b);

if (numA > numB)
    Console.WriteLine("NUMERIS A DIDESNIS UZ B");
else if (numA == numB)
    Console.WriteLine("NUMERIS B LYGUS SU A");
else
    Console.WriteLine("NUMERIS B MAZESNIS UZ A");



