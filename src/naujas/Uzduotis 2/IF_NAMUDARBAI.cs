﻿Console.WriteLine("NAUDOTI 24 VAL LAIKA");

Console.Write("VALANDA: ");
var hr = Console.ReadLine();
Console.Write("MINUTES: ");
var min = Console.ReadLine(); // Laikas praejes nuo vidurnakcio

var hrA = int.Parse(hr);
var minA = int.Parse(min);


var hrB = hrA * 60; // Paversti valandas i minutes

var laikasV = hrB + minA;

if (laikasV <= 1440)
    Console.WriteLine($"NUO VIDURNAKCIO PRAEJO {laikasV} MINUTES");
else
    Console.WriteLine("JUSU IVESTAS LAIKAS YRA NETEISINGAS");




Console.Write("METAI: ");  // Keliamuju metu patikrinimas
var metai = Console.ReadLine();

var metaiA = int.Parse(metai);

if (DateTime.IsLeapYear(metaiA))
    Console.WriteLine("KELIAMIEJI METAI");
else
    Console.WriteLine("NEKELIAMIEJI METAI");




Console.Write("NUMERIS A: ");  
var a = Console.ReadLine();
Console.Write("NUMERIS B: ");
var b = Console.ReadLine();
Console.Write("NUMERIS C: ");
var c = Console.ReadLine();

var numA = int.Parse(a);
var numB = int.Parse(b);
var numC = int.Parse(c);

if (numA + numB + numC == 180)  // Patrikrinimas ar is a b c eina sudaryti trikampi
    Console.WriteLine("IS ATKARPU EITU SUDARYTI TRIKAMPI");
else
    Console.WriteLine("IS ATKARPU NEITU SUDARYTI TRIKAMPI");

if (numA > numB && numA > numC) // Didziausio numerio isvedimas
    Console.WriteLine("NUMERIS A DIDZIAUSAS");
else if (numB > numA && numB > numC)
    Console.WriteLine("NUMERIS B DIDZIAUSAS");
else if (numC > numA && numC > numB)
    Console.WriteLine("NUMERIS C DIDZIAUSAS");
else
    Console.WriteLine("VIENAS AR KELI NUMERIAI LYGUS");
